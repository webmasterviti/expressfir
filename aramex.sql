CREATE SCHEMA aramex;

CREATE TABLE aramex.box ( 
	id                   int UNSIGNED NOT NULL  AUTO_INCREMENT,
	goods_value          varchar(64)    ,
	pickup_by            varchar(64)    ,
	shield_value         varchar(64)    ,
	weight               float UNSIGNED   ,
	charg_wt             varchar(64)    ,
	pcs                  int UNSIGNED   ,
	cube                 varchar(64)    ,
	factor               varchar(32)    ,
	product_1            varchar(64)    ,
	due_dt               varchar(100)    ,
	pickup_dt            varchar(64)    ,
	loc                  varchar(100)    ,
	remarks              varchar(255)    ,
	goods_org            varchar(64)    ,
	description          varchar(255)    ,
	product_2            varchar(64)    ,
	CONSTRAINT pk_box_id PRIMARY KEY ( id )
 ) engine=InnoDB;

CREATE INDEX idx_box_id ON aramex.box ( id );

CREATE TABLE aramex.consignee ( 
	id                   int UNSIGNED NOT NULL  AUTO_INCREMENT,
	account              varchar(64)    ,
	contry               varchar(64)    ,
	phone                varchar(32)    ,
	phone2               varchar(32)    ,
	name_c               varchar(100)    ,
	attn                 varchar(100)    ,
	ref                  varchar(64)    ,
	ref2                 varchar(64)    ,
	address              varchar(100)    ,
	city                 varchar(64)    ,
	state                varchar(64)    ,
	zip                  varchar(64)    ,
	ukr_address_id       varchar(32)    ,
	ukr_client_id        varchar(32)    ,
	CONSTRAINT pk_consignee_id PRIMARY KEY ( id )
 ) engine=InnoDB;

CREATE INDEX idx_consignee_id ON aramex.consignee ( id );

CREATE TABLE aramex.destination_entity ( 
	id                   int UNSIGNED NOT NULL  AUTO_INCREMENT,
	destination_entity   varchar(100)    ,
	CONSTRAINT pk_destination_entity_id PRIMARY KEY ( id )
 );

CREATE TABLE aramex.express_mawb ( 
	id                   int UNSIGNED NOT NULL  AUTO_INCREMENT,
	al_code              varchar(32)    ,
	flt_no               varchar(32)    ,
	etd                  varchar(64)    ,
	etd_time             varchar(32)    ,
	eta                  varchar(64)    ,
	eta_time             varchar(32)    ,
	via                  varchar(64)    ,
	CONSTRAINT pk_express_id PRIMARY KEY ( id )
 ) engine=InnoDB;

CREATE TABLE aramex.exspress_hawb ( 
	id                   int UNSIGNED NOT NULL  AUTO_INCREMENT,
	al_code              varchar(32)    ,
	flt_no               varchar(32)    ,
	etd                  varchar(32)    ,
	etd_time             varchar(32)    ,
	eta                  varchar(32)    ,
	eta_time             varchar(32)    ,
	via                  varchar(32)    ,
	CONSTRAINT pk_exspress_hawb_id PRIMARY KEY ( id )
 ) engine=InnoDB;

CREATE TABLE aramex.money ( 
	id                   int UNSIGNED NOT NULL  AUTO_INCREMENT,
	payment              varchar(128)    ,
	srn_no               varchar(128)    ,
	collection_ref       varchar(64)    ,
	collect_amt          varchar(64)    ,
	cod_amt              varchar(64)    ,
	cash_amt             varchar(64)    ,
	awb_reference        varchar(100)    ,
	CONSTRAINT pk_money_id PRIMARY KEY ( id )
 ) engine=InnoDB;

CREATE INDEX idx_money_id ON aramex.money ( id );

CREATE INDEX idx_money_id_0 ON aramex.money ( id );

CREATE TABLE aramex.snipper ( 
	id                   int UNSIGNED NOT NULL  AUTO_INCREMENT,
	account              varchar(32)    ,
	contry               varchar(16)    ,
	phone                varchar(32)    ,
	phone2               varchar(32)    ,
	name_s               varchar(100)    ,
	sent_by              varchar(100)    ,
	ref                  varchar(64)    ,
	ref2                 varchar(64)    ,
	address              varchar(100)    ,
	city                 varchar(64)    ,
	state                varchar(64)    ,
	zip                  int UNSIGNED   ,
	ukr_address_id       varchar(32)    ,
	ukr_client_id        varchar(32)    ,
	CONSTRAINT pk_snipper_id PRIMARY KEY ( id )
 ) engine=InnoDB;

CREATE INDEX idx_snipper_id ON aramex.snipper ( id );

CREATE TABLE aramex.`user` ( 
	id                   int UNSIGNED NOT NULL  AUTO_INCREMENT,
	login                varchar(100)    ,
	password             varchar(255)    ,
	firstname            varchar(100)    ,
	lastname             varchar(100)    ,
	law                  varchar(32)    ,
	CONSTRAINT pk_user_id PRIMARY KEY ( id )
 ) engine=InnoDB;

CREATE TABLE aramex.mawb ( 
	id                   bigint  NOT NULL  AUTO_INCREMENT,
	org_entity           varchar(64)    ,
	org_port             varchar(32)    ,
	carrier              varchar(64)    ,
	mawn_no              varchar(32)    ,
	origin_branch        varchar(255)    ,
	destination_port     varchar(255)    ,
	destination_entity   varchar(64)    ,
	branch               varchar(100)    ,
	mode_of_transport    varchar(255)    ,
	no_of_baby_bags      varchar(64)    ,
	no_of_master_bags    varchar(64)    ,
	ttc_bags_weight      varchar(64)    ,
	actual_weight        varchar(64)    ,
	hawbs_weight         varchar(64)    ,
	hawbs_cube           varchar(64)    ,
	remarks              varchar(64)    ,
	linehaul_supplier    varchar(255)    ,
	srr                  varchar(255)    ,
	custom_reference     varchar(255)    ,
	exspress_id          int UNSIGNED   ,
	user_id              int UNSIGNED   ,
	sub                  varchar(32)    ,
	CONSTRAINT pk_mawb_id PRIMARY KEY ( id )
 ) engine=InnoDB;

CREATE INDEX idx_mawb_exspress_id ON aramex.mawb ( exspress_id );

CREATE INDEX idx_mawb_user_id ON aramex.mawb ( user_id );

CREATE TABLE aramex.hawb ( 
	id                   bigint  NOT NULL  AUTO_INCREMENT,
	origin               varchar(32)    ,
	destination          varchar(32)    ,
	manifest             varchar(32)    ,
	snipper_id           int UNSIGNED   ,
	consignee_id         int UNSIGNED   ,
	box_id               int UNSIGNED   ,
	money_id             int UNSIGNED   ,
	mawb_id              varchar(32)    ,
	ukr_shipment_id      varchar(32)    ,
	express_id           int UNSIGNED   ,
	excel                bool    ,
	CONSTRAINT pk_hawb_id PRIMARY KEY ( id )
 ) engine=InnoDB;

CREATE INDEX idx_hawb_snipper_id ON aramex.hawb ( snipper_id );

CREATE INDEX idx_hawb_consignee_id ON aramex.hawb ( consignee_id );

CREATE INDEX idx_hawb_box_id ON aramex.hawb ( box_id );

CREATE INDEX idx_hawb_money_id ON aramex.hawb ( money_id );

CREATE INDEX idx_hawb_mawb_id ON aramex.hawb ( mawb_id );

CREATE INDEX idx_hawb_express_id ON aramex.hawb ( express_id );

ALTER TABLE aramex.hawb ADD CONSTRAINT fk_hawb_snipper FOREIGN KEY ( snipper_id ) REFERENCES aramex.snipper( id ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE aramex.hawb ADD CONSTRAINT fk_hawb_consignee FOREIGN KEY ( consignee_id ) REFERENCES aramex.consignee( id ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE aramex.hawb ADD CONSTRAINT fk_hawb_box FOREIGN KEY ( box_id ) REFERENCES aramex.box( id ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE aramex.hawb ADD CONSTRAINT fk_hawb_money FOREIGN KEY ( money_id ) REFERENCES aramex.money( id ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE aramex.hawb ADD CONSTRAINT fk_hawb_exspress_hawb FOREIGN KEY ( express_id ) REFERENCES aramex.exspress_hawb( id ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE aramex.hawb ADD CONSTRAINT fk_hawb_mawb FOREIGN KEY ( mawb_id ) REFERENCES aramex.mawb( id ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE aramex.mawb ADD CONSTRAINT fk_mawb_express FOREIGN KEY ( exspress_id ) REFERENCES aramex.express_mawb( id ) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE aramex.mawb ADD CONSTRAINT fk_mawb_user FOREIGN KEY ( user_id ) REFERENCES aramex.`user`( id ) ON DELETE NO ACTION ON UPDATE NO ACTION;
