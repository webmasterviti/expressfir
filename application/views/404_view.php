
<!--================4o4 Area =================-->
<section class="error_area">
    <div class="container">
        <div class="error_content_inner">
            <img src="img/error.png" alt="">
            <h1>404</h1>
            <h3>Sorry page not found</h3>
            <a class="more_btn" href="/">Go to home page <i class="fa fa-angle-right"></i></a>
        </div>
    </div>
</section>
<!--================End 404 Area =================-->

