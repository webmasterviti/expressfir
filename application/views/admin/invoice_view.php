<?php header('Content-Type: text/html; charset=utf-8'); ?>
    <!DOCTYPE html>
    <html>
    <head>
        <title>Накладная</title>
    </head>
    <link  rel="stylesheet" type="text/css" href="css/invoice.css">
    <body>

    <?php
    $pkgs = array(
        array('origin' => $data['search'][0], 'sku' => $data['search'][1], 'ups' => $data['search'][2], 'date' => $data['search'][3], 'ppx' => $data['search'][4],'exp' => $data['search'][5], 'payment' => $data['search'][6], 'weight' => $data['search'][7], "pieces"=> "1", "value"=> "50.00 USD", "chargeable" => "0.35 KG", "description" => "Wrist Watch(gift)"),
        array('origin' => 'UKR', 'sku' => '19573252416', 'ups' => 'UPS', 'date' => '02/16/2018', 'ppx' => 'PPX','exp' => 'EXP', 'payment' => 'P', 'weight' => '0.35 KG', "pieces"=> "1", "value"=> "50.00 USD", "chargeable" => "0.35 KG", "description" => "Wrist Watch(gift)"),
    );
    ?>


    <?php foreach ($pkgs as $item): ?>
        <div class="b-sticker">
            <table>
                <tr>
                    <img class="img" src="img/aramex.png" alt="">
                    <td width="20%" class="customer-info">
                        <div> Origin:</div>
                        <div class="seller"><h2><?php echo $item['origin'] ?></h2></div>
                    </td>
                    <td width="70%">
                        <div class="barcode"><?php echo barcode::code39($item['sku']); ?></div>
                    </td>
                </tr>
            </table>
            <table>
                <tr class="line2">
                    <td width="50%">
                        <div><h1>Destination: <?php echo $item['ups'] ?></h1></div>
                    </td>
                    <td width="40%">
                        <div><p>Pickup Date: <?php echo $item['date'] ?></p></div>
                    </td>
                </tr>
            </table>
            <table>
                <tr class="line2">
                    <td class="right" width="33%">
                        <div><h1> <?php echo $item['exp'] ?></h1></div>
                    </td>
                    <td class="right" width="33%">
                        <div><h1> <?php echo $item['ppx'] ?></h1></div>
                    </td>
                    <td width="33%">
                        <div><p> Payment: <span class="span"> <?php echo $item['payment'] ?></span></p></div>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td class="right" width="80%">
                        <div class="line3">
                            <div class="display">
                                <div>Weight: <?php echo $item['weight'] ?> </div>
                                <div> Pieces: <?php echo $item['pieces'] ?> </div>
                                <div>Value: <?php echo $item['value'] ?></div>
                            </div>
                            <br>
                            <div class="display2">
                                <div>Description: <?php echo $item['description'] ?></div>
                                <div>Chargeable: <?php echo $item['chargeable'] ?> </div>
                            </div>
                        </div>
                        <div class="line3 ot">
                            <div><h4>From</h4></div>
                            <div>Account</div>
                            <div>Oleg Georgiy</div>
                            <div>Oleg Georgiy</div>
                            <div>61, Generala petrova str.</div>
                            <div>UA</div>
                            <div>Tel: 0000</div>
                        </div>
                        <br>
                        <div class="line3 ot">
                            <div><h4>To</h4></div>
                            <div><strong>Account</strong></div>
                            <div><strong>Oleg Georgiy</strong></div>
                            <div><strong>Oleg Georgiy</strong></div>
                            <div><strong>61, Generala petrova str.</strong></div>
                            <div><strong>A</strong></div>
                            <div><strong>Tel: 0000</strong></div>
                        </div>
                    </td>
                    <td width="20%">
                        <div class="display3">
                            <div class="barcode1"><?php echo barcode::code39($item['sku']) ?>
                            </div>
                        </div>
                    </td>

                </tr>
            </table>
            <table>
                <div class="display2">
                    <div>Shpr Ref:</div>
                    <div>Cons Ref:</div>
                </div>
            </table>
        </div>


    <?php endforeach; ?>

    </body>
    </html>

<?php
class barcode {

    protected static $code39 = array(
        '0' => 'bwbwwwbbbwbbbwbw', '1' => 'bbbwbwwwbwbwbbbw',
        '2' => 'bwbbbwwwbwbwbbbw', '3' => 'bbbwbbbwwwbwbwbw',
        '4' => 'bwbwwwbbbwbwbbbw', '5' => 'bbbwbwwwbbbwbwbw',
        '6' => 'bwbbbwwwbbbwbwbw', '7' => 'bwbwwwbwbbbwbbbw',
        '8' => 'bbbwbwwwbwbbbwbw', '9' => 'bwbbbwwwbwbbbwbw',
        'A' => 'bbbwbwbwwwbwbbbw', 'B' => 'bwbbbwbwwwbwbbbw',
        'C' => 'bbbwbbbwbwwwbwbw', 'D' => 'bwbwbbbwwwbwbbbw',
        'E' => 'bbbwbwbbbwwwbwbw', 'F' => 'bwbbbwbbbwwwbwbw',
        'G' => 'bwbwbwwwbbbwbbbw', 'H' => 'bbbwbwbwwwbbbwbw',
        'I' => 'bwbbbwbwwwbbbwbw', 'J' => 'bwbwbbbwwwbbbwbw',
        'K' => 'bbbwbwbwbwwwbbbw', 'L' => 'bwbbbwbwbwwwbbbw',
        'M' => 'bbbwbbbwbwbwwwbw', 'N' => 'bwbwbbbwbwwwbbbw',
        'O' => 'bbbwbwbbbwbwwwbw', 'P' => 'bwbbbwbbbwbwwwbw',
        'Q' => 'bwbwbwbbbwwwbbbw', 'R' => 'bbbwbwbwbbbwwwbw',
        'S' => 'bwbbbwbwbbbwwwbw', 'T' => 'bwbwbbbwbbbwwwbw',
        'U' => 'bbbwwwbwbwbwbbbw', 'V' => 'bwwwbbbwbwbwbbbw',
        'W' => 'bbbwwwbbbwbwbwbw', 'X' => 'bwwwbwbbbwbwbbbw',
        'Y' => 'bbbwwwbwbbbwbwbw', 'Z' => 'bwwwbbbwbbbwbwbw',
        '-' => 'bwwwbwbwbbbwbbbw', '.' => 'bbbwwwbwbwbbbwbw',
        ' ' => 'bwwwbbbwbwbbbwbw', '*' => 'bwwwbwbbbwbbbwbw',
        '$' => 'bwwwbwwwbwwwbwbw', '/' => 'bwwwbwwwbwbwwwbw',
        '+' => 'bwwwbwbwwwbwwwbw', '%' => 'bwbwwwbwwwbwwwbw'
    );

    public static function code39($text) {
        if (!preg_match('/^[A-Z0-9-. $+\/%]+$/i', $text)) {
            throw new Exception('Ошибка ввода');
        }
        $number = $text;
        $text ="*" .strtoupper($text)."*";
        $length = strlen($text);
        $chars = str_split($text);
        $colors = '';

        foreach ($chars as $char) {
            $colors .= self::$code39[$char];
        }

        $html = '
            <div style=" float:left;">
            <div>';

        foreach (str_split($colors) as $i => $color) {
            if ($color=='b') {
                $html.='<SPAN style="BORDER-LEFT: 0.02in solid; DISPLAY: inline-block; HEIGHT: 1in;"></SPAN>';
            } else {
                $html.='<SPAN style="BORDER-LEFT: white 0.02in solid; DISPLAY: inline-block; HEIGHT: 1in;"></SPAN>';
            }
        }

        $html.='</div>
            <div style="float:left; width:100%;" align=center >'.$number.'</div></div>';
        //echo htmlspecialchars($html);
        echo $html;
    }

}