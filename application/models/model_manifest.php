<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 6/4/2018
 * Time: 8:08 PM
 */
class model_manifest extends Model
{
    public function getHawb($id_hawb){
        //Create connect database
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare("SELECT * FROM hawb WHERE id =$id_hawb");
        $query->execute();
        $resultQuery = $query->fetchAll();
        $resultQuery = $resultQuery[0];
            $query = $connect->db->prepare('SELECT * FROM snipper WHERE id =' . $resultQuery['snipper_id']);
            $query->execute();
            $result_snipper = $query->fetchAll();
            $resultQuery['snipper_id'] = $result_snipper[0];
            ////////
            $query = $connect->db->prepare('SELECT * FROM consignee WHERE id =' . $resultQuery['consignee_id']);
            $query->execute();
            $result_consignee = $query->fetchAll();
            $resultQuery['consignee_id'] = $result_consignee[0];
            ////////
            $query = $connect->db->prepare('SELECT * FROM box WHERE id =' . $resultQuery['box_id']);
            $query->execute();
            $result_consignee = $query->fetchAll();
            $resultQuery['box_id'] = $result_consignee[0];
            //////
            $query = $connect->db->prepare('SELECT * FROM money WHERE id =' . $resultQuery['money_id']);
            $query->execute();
            $result_consignee = $query->fetchAll();
            $resultQuery['money_id'] = $result_consignee[0];
            //////
            try{
            $query = $connect->db->prepare('SELECT * FROM ukr_shipment_id WHERE id =' . $resultQuery['ukr_shipment_id']);
            $query->execute();
            $result_consignee = $query->fetchAll();
            $resultQuery['ukr_shipment_id'] = $result_consignee[0];
            }catch (Exception $e){

            }
            try {
                $query = $connect->db->prepare('SELECT * FROM express_id WHERE id =' . $resultQuery['express_id']);
                $query->execute();
                $result_consignee = $query->fetchAll();
                $resultQuery['express_id'] = $result_consignee[0];
            }catch (Exception $e){
            }
        return $resultQuery;
    }
    public function getAllDbMawb(){
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT * FROM mawb WHERE mawb.id');
        $query->execute();
        $resultQuery = $query->fetchAll();

        for($i = 0; $i < count($resultQuery);$i++){
            $id =$resultQuery[$i]['exspress_id'];
            $query = $connect->db->prepare("SELECT * FROM express_mawb WHERE id =$id");
            $query->execute();
            $result_express = $query->fetchAll();
            if($result_express[0]['via'] == 0){
                $result_express[0]['via'] = '';
            }
            $result_express[0] =
            $resultQuery[$i]['express_id'] = $result_express[0];
            $id = $resultQuery[$i]['id'];
            $query = $connect->db->prepare("SELECT * FROM mawb INNER JOIN hawb ON mawb.id = hawb.mawb_id WHERE mawb.id=$id");
            $query->execute();
            $resultCount = $query->fetchAll();
            for ($num = 0; $num < count($resultCount); $num++) {
                $joinHawb [] = $resultCount[$num]['id'];
            }
            $resultQuery[$i]['hawb'] = $joinHawb;
            for ($j = 0; $j < count($joinHawb); $j++) {
                $resultGetHawb = $this->getHawb($resultQuery[$i]['hawb'][$j]);
                $resultQuery[$i]['hawb'][$j] = $resultGetHawb;
            }
            unset($joinHawb);
        }
        return $resultQuery;
    }

    public function getTable($array){
        $body = '';
        for($i = 0;$i<count($array);$i++) {
            $actual_weight = explode(" ", $array[$i]['ttc_bags_weight']);
            $body .= '
         <div class="card">
            <h3 class="manifest-title">Aramex International Manifest</h3>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-3 manifest-blue">Org Port</div>
                        <div class="col-md-3 manifest-red">' . $array[$i]['org_port'] . '</div>
                        <div class="col-md-3 manifest-blue">MAWB</div>
                        <div class="col-md-3 manifest-red">' . $array[$i]['id'] . '</div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 manifest-blue">Dest Port</div>
                        <div class="col-md-3 manifest-red">' . $array[$i]['destination_port'] . '</div>
                        <div class="col-md-3 manifest-blue">Remarks</div>
                        <div class="col-md-3 manifest-red">' . $array[$i]['remarks'] . '</div>
                    </div>
                </div>
                <div class="col-md-6">
                <div class="row">
                    <div class="col-md-2 manifest-blue">FLT</div>
                    <div class="col-md-2 manifest-red">' . $array[$i]['express_id']['flt_no'] . '</div>
                    <div class="col-md-4 manifest-blue">Total No. of Shipments</div>
                    <div class="col-md-3 manifest-red">' . count($array[$i][hawb]) . '</div>
                </div>

                <div class="row">
                    <div class="col-md-2 manifest-blue">VIA</div>
                    <div class="col-md-2 manifest-red">' . $array[$i]['express_id']['via'] . '</div>
                    <div class="col-md-4 manifest-blue">Total No. of Master Bags</div>
                    <div class="col-md-3 manifest-red">' . $array[$i]['no_of_master_bags'] . '</div>
                </div>

                <div class="row">
                    <div class="col-md-2 manifest-blue">ETD</div>
                    <div class="col-md-2 manifest-red">' . $array[$i]['express_id']['etd'] . ' ' . $array[$i]['express_id']['etd_time'] . '</div>
                    <div class="col-md-4 manifest-blue">Total No. of Baby Bags</div>
                    <div class="col-md-3 manifest-red">' . $array[$i]['no_of_baby_bags'] . '</div>
                </div>

                <div class="row">
                    <div class="col-md-2 manifest-blue">ETA</div>
                    <div class="col-md-2 manifest-red">' . $array[$i]['express_id']['eta'] . ' ' . $array[$i]['express_id']['eta_time'] . '</div>
                    <div class="col-md-4 manifest-blue">Total Shipments Weight</div>
                    <div class="col-md-3 manifest-red">' . $array[$i]['ttc_bags_weight'] . '</div>
                </div>

                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-4 manifest-blue">Consol Weight</div>
                    <div class="col-md-3 manifest-red">' . round($actual_weight[0]) . ' ' . $actual_weight[1] . '</div>

                </div>
                </div>
            </div>
            <div class="row">
            <div class="col-md-2 manifest-blue">
                Dest Country
            </div>
            <div class="col-md-1 manifest-red">
                ' . $array[$i]['hawb'][0]['consignee_id']['contry'] . '
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-2 manifest-blue">
                Total Weight
            </div>
            <div class="col-md-2 manifest-red">
                ' . $array[$i]['ttc_bags_weight'] . '
            </div>
            <div class="col-md-2 manifest-button-detail">
                <a data-toggle="collapse" href="#collapseExample'.$i.'" role="button" aria-expanded="false" aria-controls="collapseExample'.$i.'">
                    <img style="height: 25px;width: 25px" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTI7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgo8cGF0aCBzdHlsZT0iZmlsbDojOTM0ODJCOyIgZD0iTTM3NS4zNTksMjAuODE4Yy0xMS4wNjQsMC0yNjguOTgyLDAtMjgzLjI3NSwwYy0xOC42NjYsMC0zMy45MzksMTUuMjcyLTMzLjkzOSwzMy45MzkgIGMwLDMuNTg3LDAsMzMwLjY0LDAsMzQ0Ljg3MXY2NS4yNDJjMCwxOC42NjYsMTUuMjcyLDMzLjkzOSwzMy45MzksMzMuOTM5aDcuNzcyYzI5LjkwMiwwLDI0Mi40NjIsMCwyNzUuNTA0LDAgIGMxOC42NjYsMCwzMy45MzktMTUuMjcyLDMzLjkzOS0zMy45MzljMC0xMi40MjgsMC0zOTkuMTc4LDAtNDEwLjExM0M0MDkuMjk4LDM2LjA5LDM5NC4wMjUsMjAuODE4LDM3NS4zNTksMjAuODE4eiIvPgo8cGF0aCBzdHlsZT0iZmlsbDojODczOTIxOyIgZD0iTTg2LjYxNSw0NjQuODd2LTY1LjI0MmMwLTE0LjIzMSwwLTM0MS4yODQsMC0zNDQuODcxYzAtMTguNjY2LDE1LjI3Mi0zMy45MzksMzMuOTM5LTMzLjkzOSAgYy02LjY3OCwwLTIxLjc5NiwwLTI4LjQ3LDBjLTE4LjY2NiwwLTMzLjkzOSwxNS4yNzItMzMuOTM5LDMzLjkzOWMwLDMuNTg3LDAsMzMwLjY0LDAsMzQ0Ljg3MXY2NS4yNDIgIGMwLDE4LjY2NiwxNS4yNzIsMzMuOTM5LDMzLjkzOSwzMy45MzljNi42NzYsMCwyMS43OTUsMCwyOC40NywwQzEwMS44ODgsNDk4LjgwOSw4Ni42MTUsNDgzLjUzNiw4Ni42MTUsNDY0Ljg3eiIvPgo8cGF0aCBzdHlsZT0iZmlsbDojRUFFNEQ2OyIgZD0iTTM2OC4zLDQ4Ljg0NGMtMTkuNjg5LDAtMTg1LjUxNywwLTE5MS4wNDUsMGgtMTIuMzU4Yy03MC45ODYsMC4wOTEtNjguMjU3LTAuOTcxLTcyLjgyMSwyLjExOCAgYy0yLjgyNCwxLjkxMS00LjYwMSw0LjgyOS01LjIsNy43NTZjLTAuNDE4LDIuMDI2LTAuMjQ1LDQwMC4yNDEtMC4xNjgsNDAwLjg4NGMwLjcyNyw2LjIxMSw1Ljc4NCw5LjA4Niw1LjM2OCw4LjgwNSAgYzQuOTM4LDMuMzQxLTguMzEzLDIuMTE4LDE5Ny45NDIsMi4xMThjMjYuMTI3LDAsNTYuMTQyLDAsNzguMjgyLDBjNi43OTcsMCwxMi4zNTgtNS41NjEsMTIuMzU4LTEyLjM1OCAgYzAtMTIuNjc4LDAtMzcyLjA5NywwLTM5Ni45NjdDMzgwLjY1Nyw1NC40MDUsMzc1LjA5Nyw0OC44NDQsMzY4LjMsNDguODQ0eiIvPgo8cGF0aCBzdHlsZT0iZmlsbDojREREM0MwOyIgZD0iTTExNy41MDksNDU4LjE2OGMwLjQ4OC00MjYuNDg3LTIuNTcxLTQwMC4xNTUsNC4xNDItNDA2LjE2YzIuNDAxLTIuMTUxLDUuMzA0LTMuMTY0LDguMjE1LTMuMTY0ICBjLTMyLjY0NiwwLjA4Ny0zMS4yMDItMC4xOTYtMzMuMzc2LDAuMjUyYy00Ljc1NywwLjk4LTguNjExLDQuNzQ2LTkuNjE1LDkuNjIyYy0wLjYzOCwzLjA5NC0wLjA2NC0xMy45NzktMC4yNiwzOTkuNDUgIGMwLDcuNDMxLDYuNDU3LDEwLjkxNCw1LjQ2MSwxMC4yNGM0LjE1NCwyLjgxMiwzLjY0MywyLjA5NSwzNy43OTEsMi4xMThDMTIyLjg4NSw0NzAuNTI2LDExNy41MDksNDY0Ljc1MywxMTcuNTA5LDQ1OC4xNjh6Ii8+CjxwYXRoIHN0eWxlPSJmaWxsOiNDQ0NDQ0M7IiBkPSJNMjczLjAxNCw4OC4zMzhoLTc4Ljc1N2MtMTEuNzAzLDAtMjEuMjI1LTkuNTIxLTIxLjIyNS0yMS4yMjRWNDMuNjk1ICBjMC00LjI2NiwzLjQ1OC03LjcyMyw3LjcyMy03LjcyM2M0LjI2NiwwLDcuNzIzLDMuNDU4LDcuNzIzLDcuNzIzdjIzLjQxOGMwLDMuMTg2LDIuNTkyLDUuNzc3LDUuNzc4LDUuNzc3aDc4Ljc1NyAgYzMuMTg2LDAsNS43NzgtMi41OTIsNS43NzgtNS43NzdWNDMuNjk1YzAtNC4yNjYsMy40NTgtNy43MjMsNy43MjMtNy43MjNjNC4yNjYsMCw3LjcyNCwzLjQ1OCw3LjcyNCw3LjcyM3YyMy40MTggIEMyOTQuMjM5LDc4LjgxNywyODQuNzE4LDg4LjMzOCwyNzMuMDE0LDg4LjMzOHoiLz4KPGc+Cgk8cGF0aCBzdHlsZT0iZmlsbDojQzlCODlGOyIgZD0iTTMyMS44NzYsMTYxLjA3M0gxNzcuNDQ5Yy00LjI2NiwwLTcuNzIzLTMuNDU4LTcuNzIzLTcuNzIzczMuNDU4LTcuNzIzLDcuNzIzLTcuNzIzaDE0NC40MjcgICBjNC4yNjYsMCw3LjcyMywzLjQ1OCw3LjcyMyw3LjcyM1MzMjYuMTQyLDE2MS4wNzMsMzIxLjg3NiwxNjEuMDczeiIvPgoJPHBhdGggc3R5bGU9ImZpbGw6I0M5Qjg5RjsiIGQ9Ik0zMjEuODc2LDE5OC4xNDNIMTc3LjQ0OWMtNC4yNjYsMC03LjcyMy0zLjQ1OC03LjcyMy03LjcyM2MwLTQuMjY2LDMuNDU4LTcuNzIzLDcuNzIzLTcuNzIzICAgaDE0NC40MjdjNC4yNjYsMCw3LjcyMywzLjQ1OCw3LjcyMyw3LjcyM0MzMjkuNiwxOTQuNjg2LDMyNi4xNDIsMTk4LjE0MywzMjEuODc2LDE5OC4xNDN6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojQzlCODlGOyIgZD0iTTMyMS44NzYsMjcyLjI4NkgxNzcuNDQ5Yy00LjI2NiwwLTcuNzIzLTMuNDU3LTcuNzIzLTcuNzI0YzAtNC4yNjYsMy40NTgtNy43MjMsNy43MjMtNy43MjMgICBoMTQ0LjQyN2M0LjI2NiwwLDcuNzIzLDMuNDU4LDcuNzIzLDcuNzIzQzMyOS42LDI2OC44MjgsMzI2LjE0MiwyNzIuMjg2LDMyMS44NzYsMjcyLjI4NnoiLz4KCTxwYXRoIHN0eWxlPSJmaWxsOiNDOUI4OUY7IiBkPSJNMzIxLjg3NiwyMzUuMjE1SDE3Ny40NDljLTQuMjY2LDAtNy43MjMtMy40NTgtNy43MjMtNy43MjNzMy40NTgtNy43MjMsNy43MjMtNy43MjNoMTQ0LjQyNyAgIGM0LjI2NiwwLDcuNzIzLDMuNDU4LDcuNzIzLDcuNzIzUzMyNi4xNDIsMjM1LjIxNSwzMjEuODc2LDIzNS4yMTV6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojQzlCODlGOyIgZD0iTTMyMS44NzYsMzA5LjM1N0gxNzcuNDQ5Yy00LjI2NiwwLTcuNzIzLTMuNDU3LTcuNzIzLTcuNzI0czMuNDU4LTcuNzIzLDcuNzIzLTcuNzIzaDE0NC40MjcgICBjNC4yNjYsMCw3LjcyMywzLjQ1Nyw3LjcyMyw3LjcyM1MzMjYuMTQyLDMwOS4zNTcsMzIxLjg3NiwzMDkuMzU3eiIvPgoJPHBhdGggc3R5bGU9ImZpbGw6I0M5Qjg5RjsiIGQ9Ik0zMjEuODc2LDM4My40OTlIMTc3LjQ0OWMtNC4yNjYsMC03LjcyMy0zLjQ1Ny03LjcyMy03LjcyNGMwLTQuMjY2LDMuNDU4LTcuNzIzLDcuNzIzLTcuNzIzICAgaDE0NC40MjdjNC4yNjYsMCw3LjcyMywzLjQ1OCw3LjcyMyw3LjcyM0MzMjkuNiwzODAuMDQyLDMyNi4xNDIsMzgzLjQ5OSwzMjEuODc2LDM4My40OTl6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojQzlCODlGOyIgZD0iTTMyMS44NzYsMzQ2LjQyOUgxNzcuNDQ5Yy00LjI2NiwwLTcuNzIzLTMuNDU3LTcuNzIzLTcuNzI0czMuNDU4LTcuNzIzLDcuNzIzLTcuNzIzaDE0NC40MjcgICBjNC4yNjYsMCw3LjcyMywzLjQ1Nyw3LjcyMyw3LjcyM1MzMjYuMTQyLDM0Ni40MjksMzIxLjg3NiwzNDYuNDI5eiIvPgoJPHBhdGggc3R5bGU9ImZpbGw6I0M5Qjg5RjsiIGQ9Ik0xNDUuNTIzLDE2MS4wNzFjLTAuNTA1LDAtMS4wMDktMC4wNTItMS41MDQtMC4xNDRjLTAuNDk0LTAuMTAzLTAuOTc5LTAuMjQ3LTEuNDUyLTAuNDQzICAgYy0wLjQ2NC0wLjE5Ni0wLjkwNi0wLjQzMi0xLjMyOC0wLjcxYy0wLjQyMi0wLjI4OC0wLjgxNC0wLjYwNy0xLjE3NC0wLjk1N2MtMC4zNi0wLjM2MS0wLjY3OS0wLjc1Mi0wLjk1Ny0xLjE3NCAgIHMtMC41MjYtMC44NzUtMC43MTEtMS4zMzljLTAuMTk2LTAuNDYzLTAuMzQtMC45NDctMC40NDMtMS40NDJjLTAuMTAzLTAuNTA0LTAuMTU0LTEuMDA5LTAuMTU0LTEuNTE0ICAgYzAtMi4wMjksMC44MjQtNC4wMjcsMi4yNjUtNS40NThjMC4zNjEtMC4zNiwwLjc1Mi0wLjY3OSwxLjE3NC0wLjk1N3MwLjg2NS0wLjUyNSwxLjMyOC0wLjcxMWMwLjQ3NC0wLjE5NiwwLjk1OC0wLjM0LDEuNDUyLTAuNDQzICAgYzAuOTk5LTAuMjA2LDIuMDE4LTAuMjA2LDMuMDE3LDBjMC40OTQsMC4xMDMsMC45NzksMC4yNDcsMS40NDIsMC40NDNjMC40NjMsMC4xODUsMC45MTYsMC40MzMsMS4zMzksMC43MTEgICBjMC40MjIsMC4yNzgsMC44MTQsMC41OTcsMS4xNzQsMC45NTdjMS40MzEsMS40MzIsMi4yNTUsMy40MjksMi4yNTUsNS40NThjMCwwLjUwNC0wLjA1MiwxLjAwOS0wLjE0NCwxLjUxNCAgIGMtMC4xMDMsMC40OTQtMC4yNDcsMC45NzktMC40NDMsMS40NDJjLTAuMTk2LDAuNDY0LTAuNDMyLDAuOTE3LTAuNzEsMS4zMzljLTAuMjg4LDAuNDIyLTAuNjA3LDAuODE0LTAuOTU3LDEuMTc0ICAgYy0wLjM2MSwwLjM1LTAuNzUyLDAuNjY5LTEuMTc0LDAuOTU3Yy0wLjQyMiwwLjI3OC0wLjg3NSwwLjUxNS0xLjMzOSwwLjcxYy0wLjQ2NCwwLjE5Ni0wLjk0OCwwLjM0LTEuNDQyLDAuNDQzICAgQzE0Ni41MzIsMTYxLjAxOSwxNDYuMDI3LDE2MS4wNzEsMTQ1LjUyMywxNjEuMDcxeiIvPgoJPHBhdGggc3R5bGU9ImZpbGw6I0M5Qjg5RjsiIGQ9Ik0xNDUuNTIzLDE5OC4xNDNjLTAuNTA1LDAtMS4wMDktMC4wNTItMS41MDQtMC4xNTRjLTAuNDk0LTAuMDkzLTAuOTc5LTAuMjQ3LTEuNDUyLTAuNDMzICAgYy0wLjQ2NC0wLjE5Ni0wLjkwNi0wLjQzMi0xLjMyOC0wLjcxYy0wLjQyMi0wLjI4OC0wLjgyNC0wLjYwNy0xLjE3NC0wLjk2OGMtMC4zNi0wLjM1LTAuNjc5LTAuNzUyLTAuOTU3LTEuMTc0ICAgYy0wLjI3OC0wLjQxMi0wLjUyNi0wLjg2NS0wLjcxMS0xLjMyOGMtMC4xOTYtMC40NjMtMC4zNC0wLjk1Ny0wLjQ0My0xLjQ0MmMtMC4xMDMtMC41MDQtMC4xNTQtMS4wMDktMC4xNTQtMS41MTQgICBzMC4wNTEtMS4wMDksMC4xNTQtMS41MTRjMC4xMDMtMC40ODQsMC4yNDctMC45NzgsMC40NDMtMS40NDJjMC4xODUtMC40NjQsMC40MzMtMC45MTcsMC43MTEtMS4zMjggICBjMC4yNzgtMC40MjIsMC41OTctMC44MjQsMC45NTctMS4xNzRjMC4zNS0wLjM2LDAuNzUyLTAuNjc5LDEuMTc0LTAuOTY4YzAuNDIyLTAuMjc4LDAuODY1LTAuNTE1LDEuMzI4LTAuNzEgICBjMC40NzQtMC4xODYsMC45NTgtMC4zNCwxLjQ1Mi0wLjQzMmMwLjk4OS0wLjIwNiwyLjAxOC0wLjIwNiwzLjAxNywwYzAuNDk0LDAuMDkyLDAuOTc5LDAuMjQ3LDEuNDQyLDAuNDMyICAgYzAuNDYzLDAuMTk2LDAuOTE2LDAuNDMyLDEuMzM5LDAuNzFjMC40MjIsMC4yODksMC44MTQsMC42MDgsMS4xNzQsMC45NjhjMC4zNSwwLjM1LDAuNjc5LDAuNzUyLDAuOTU3LDEuMTc0ICAgYzAuMjc4LDAuNDEyLDAuNTE1LDAuODY1LDAuNzEsMS4zMjhjMC4xOTYsMC40NjQsMC4zNCwwLjk1OCwwLjQ0MywxLjQ0MmMwLjA5MywwLjUwNSwwLjE0NCwxLjAwOSwwLjE0NCwxLjUxNCAgIGMwLDAuNTA0LTAuMDUyLDEuMDA5LTAuMTQ0LDEuNTE0Yy0wLjEwMywwLjQ4NC0wLjI0NywwLjk3OS0wLjQ0MywxLjQ0MmMtMC4xOTYsMC40NjQtMC40MzIsMC45MTctMC43MSwxLjMyOCAgIGMtMC4yNzgsMC40MjItMC42MDcsMC44MjQtMC45NTcsMS4xNzRjLTAuMzYxLDAuMzYxLTAuNzUyLDAuNjgtMS4xNzQsMC45NjhjLTAuNDIyLDAuMjc4LTAuODc1LDAuNTE1LTEuMzM5LDAuNzEgICBjLTAuNDY0LDAuMTg2LTAuOTQ4LDAuMzQtMS40NDIsMC40MzNDMTQ2LjUzMiwxOTguMDkyLDE0Ni4wMjcsMTk4LjE0MywxNDUuNTIzLDE5OC4xNDN6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojQzlCODlGOyIgZD0iTTE0NS41MjMsMjcyLjI4OWMtMi4wMjksMC00LjAxNi0wLjgyNC01LjQ1OC0yLjI2NmMtMC4zNi0wLjM2LTAuNjc5LTAuNzUxLTAuOTU3LTEuMTc0ICAgYy0wLjI3OC0wLjQyMi0wLjUxNS0wLjg2NS0wLjcxMS0xLjMyOGMtMC4xOTYtMC40NzQtMC4zNC0wLjk1OC0wLjQ0My0xLjQ1MmMtMC4xMDMtMC40OTUtMC4xNTQtMC45OTktMC4xNTQtMS41MDMgICBjMC0wLjUwNSwwLjA1MS0xLjAxLDAuMTU0LTEuNTE0YzAuMTAzLTAuNDk1LDAuMjQ3LTAuOTc5LDAuNDQzLTEuNDQyYzAuMTk2LTAuNDY0LDAuNDMzLTAuOTE2LDAuNzExLTEuMzM5ICAgYzAuMjc4LTAuNDIyLDAuNTk3LTAuODE0LDAuOTU3LTEuMTc1YzAuMzUtMC4zNSwwLjc1Mi0wLjY3OSwxLjE3NC0wLjk1N3MwLjg2NS0wLjUxNSwxLjMyOC0wLjcxICAgYzAuNDc0LTAuMTk2LDAuOTU4LTAuMzQsMS40NTItMC40NDRjMC45ODktMC4xOTUsMi4wMTgtMC4xOTUsMy4wMTcsMGMwLjQ5NCwwLjEwNCwwLjk3OSwwLjI0NywxLjQ0MiwwLjQ0NCAgIGMwLjQ2MywwLjE5NSwwLjkxNiwwLjQzMiwxLjMzOSwwLjcxYzAuNDIyLDAuMjc5LDAuODE0LDAuNjA3LDEuMTc0LDAuOTU3YzAuMzUsMC4zNjEsMC42NzksMC43NTIsMC45NTcsMS4xNzUgICBjMC4yNzgsMC40MjIsMC41MTUsMC44NzUsMC43MSwxLjMzOWMwLjE5NiwwLjQ2NCwwLjM0LDAuOTQ3LDAuNDQzLDEuNDQyYzAuMDkzLDAuNTA0LDAuMTQ0LDEuMDA5LDAuMTQ0LDEuNTE0ICAgcy0wLjA1MiwxLjAwOS0wLjE0NCwxLjUwM2MtMC4xMDMsMC40OTQtMC4yNDcsMC45NzktMC40NDMsMS40NTJjLTAuMTk2LDAuNDY0LTAuNDMyLDAuOTA2LTAuNzEsMS4zMjhzLTAuNjA3LDAuODE0LTAuOTU3LDEuMTc0ICAgYy0wLjM2MSwwLjM1LTAuNzUyLDAuNjgtMS4xNzQsMC45NTdjLTAuNDIyLDAuMjc5LTAuODc1LDAuNTE1LTEuMzM5LDAuNzExYy0wLjQ2NCwwLjE5NS0wLjk0OCwwLjM0LTEuNDQyLDAuNDQzICAgQzE0Ni41MzIsMjcyLjIzOCwxNDYuMDI3LDI3Mi4yODksMTQ1LjUyMywyNzIuMjg5eiIvPgoJPHBhdGggc3R5bGU9ImZpbGw6I0M5Qjg5RjsiIGQ9Ik0xNDUuNTIzLDIzNS4yMTZjLTAuNTA1LDAtMS4wMDktMC4wNTItMS41MDQtMC4xNTRjLTAuNDk0LTAuMTAzLTAuOTc5LTAuMjQ3LTEuNDUyLTAuNDQzICAgYy0wLjQ2NC0wLjE4Ni0wLjkwNi0wLjQzMi0xLjMyOC0wLjcxMWMtMC40MjItMC4yNzgtMC44MTQtMC41OTctMS4xNzQtMC45NThjLTEuNDQyLTEuNDMyLTIuMjY1LTMuNDI5LTIuMjY1LTUuNDU4ICAgYzAtMC41MDUsMC4wNTEtMS4wMDksMC4xNTQtMS41MTRjMC4xMDMtMC40ODQsMC4yNDctMC45NzgsMC40NDMtMS40NDJzMC40MzMtMC45MTcsMC43MTEtMS4zMjggICBjMC4yNzgtMC40MjIsMC41OTctMC44MjQsMC45NTctMS4xNzRjMC4zNjEtMC4zNiwwLjc1Mi0wLjY3OSwxLjE3NC0wLjk2OGMwLjQyMi0wLjI3OCwwLjg2NS0wLjUxNSwxLjMyOC0wLjcxICAgYzAuNDc0LTAuMTg2LDAuOTU4LTAuMzQsMS40NTItMC40MzJjMi41MDItMC41MTUsNS4xNywwLjMwOSw2Ljk3MiwyLjExMWMwLjM1LDAuMzUsMC42NjksMC43NTIsMC45NTcsMS4xNzQgICBjMC4yNzgsMC40MTIsMC41MTUsMC44NjUsMC43MSwxLjMyOGMwLjE4NiwwLjQ2NCwwLjM0LDAuOTU4LDAuNDMzLDEuNDQyYzAuMTAzLDAuNTA1LDAuMTU0LDEuMDA5LDAuMTU0LDEuNTE0ICAgYzAsMi4wMjgtMC44MjQsNC4wMjYtMi4yNTUsNS40NThDMTQ5LjU0OSwyMzQuMzkyLDE0Ny41NjIsMjM1LjIxNiwxNDUuNTIzLDIzNS4yMTZ6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojQzlCODlGOyIgZD0iTTE0NS41MjMsMzA5LjM1MmMtMi4wMjksMC00LjAxNi0wLjgyNS01LjQ1OC0yLjI1NmMtMC4zNi0wLjM2LTAuNjc5LTAuNzUxLTAuOTU3LTEuMTc0ICAgYy0wLjI3OC0wLjQyMi0wLjUyNi0wLjg2NS0wLjcxMS0xLjM0Yy0wLjE5Ni0wLjQ2My0wLjM0LTAuOTQ3LTAuNDQzLTEuNDQxYy0wLjEwMy0wLjQ5NS0wLjE1NC0xLjAxLTAuMTU0LTEuNTAzICAgYzAtMC41MDUsMC4wNTEtMS4wMiwwLjE1NC0xLjUxNGMwLjEwMy0wLjQ5NSwwLjI0Ny0wLjk3OSwwLjQ0My0xLjQ0MmMwLjE4NS0wLjQ3NCwwLjQzMy0wLjkxNiwwLjcxMS0xLjMzOSAgIGMwLjI3OC0wLjQyMiwwLjU5Ny0wLjgxNCwwLjk1Ny0xLjE3NWMxLjc5Mi0xLjc5MSw0LjQ1OS0yLjYxNiw2Ljk3Mi0yLjExMWMwLjQ5NCwwLjEwNCwwLjk3OSwwLjI0NywxLjQ0MiwwLjQ0NCAgIGMwLjQ2MywwLjE5NSwwLjkxNiwwLjQzMiwxLjMyOCwwLjcxYzAuNDMzLDAuMjc5LDAuODI0LDAuNjA3LDEuMTg1LDAuOTU3YzAuMzUsMC4zNjEsMC42NjksMC43NTIsMC45NTcsMS4xNzUgICBjMC4yNzgsMC40MjIsMC41MTUsMC44NjUsMC43MSwxLjMzOWMwLjE5NiwwLjQ2NCwwLjM0LDAuOTQ3LDAuNDQzLDEuNDQyYzAuMDkzLDAuNDk0LDAuMTQ0LDEuMDA5LDAuMTQ0LDEuNTE0ICAgYzAsMi4wMjgtMC44MjQsNC4wMTYtMi4yNTUsNS40NThjLTAuMzYxLDAuMzUtMC43NTIsMC42OC0xLjE4NSwwLjk1N2MtMC40MTIsMC4yNzktMC44NjUsMC41MTUtMS4zMjgsMC43MTEgICBjLTAuNDY0LDAuMTk1LTAuOTQ4LDAuMzQtMS40NDIsMC40NDNDMTQ2LjUzMiwzMDkuMzExLDE0Ni4wMjcsMzA5LjM1MiwxNDUuNTIzLDMwOS4zNTJ6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojQzlCODlGOyIgZD0iTTE0NS41MjMsMzgzLjQ5N2MtMC41MDUsMC0xLjAwOS0wLjA1Mi0xLjUwNC0wLjE1NWMtMC40OTQtMC4wOTItMC45NzktMC4yNDctMS40NTItMC40MzIgICBjLTAuNDY0LTAuMTk2LTAuOTA2LTAuNDMyLTEuMzI4LTAuNzExYy0wLjQyMi0wLjI4OC0wLjgxNC0wLjYwNy0xLjE3NC0wLjk2N2MtMC4zNi0wLjM1LTAuNjc5LTAuNzQxLTAuOTU3LTEuMTc1ICAgYy0wLjI3OC0wLjQxMS0wLjUyNi0wLjg2NS0wLjcxMS0xLjMyOWMtMC4xOTYtMC40NjMtMC4zNC0wLjk0Ny0wLjQ0My0xLjQ0MWMtMC4xMDMtMC41MDUtMC4xNTQtMS4wMS0wLjE1NC0xLjUxMyAgIGMwLTAuNTA1LDAuMDUxLTEuMDEsMC4xNTQtMS41MDRjMC4xMDMtMC40OTUsMC4yNDctMC45NzksMC40NDMtMS40NTJjMC4xODUtMC40NjQsMC40MzMtMC45MTcsMC43MTEtMS4zMjkgICBjMC4yNzgtMC40MjIsMC41OTctMC44MjQsMC45NTctMS4xNzVjMC4zNjEtMC4zNiwwLjc1Mi0wLjY3OSwxLjE3NC0wLjk1N3MwLjg2NS0wLjUyNSwxLjMyOC0wLjcxICAgYzAuNDc0LTAuMTk2LDAuOTU4LTAuMzUxLDEuNDUyLTAuNDQ0YzAuOTg5LTAuMjA2LDIuMDE4LTAuMjA2LDMuMDE3LDBjMC40OTQsMC4wOTMsMC45NzksMC4yNDcsMS40NDIsMC40NDQgICBjMC40NjMsMC4xODUsMC45MTYsMC40MzIsMS4zMzksMC43MWMwLjQyMiwwLjI3OSwwLjgxNCwwLjU5NywxLjE3NCwwLjk1N2MwLjM1LDAuMzUxLDAuNjc5LDAuNzUyLDAuOTU3LDEuMTc1ICAgYzAuMjc4LDAuNDExLDAuNTE1LDAuODY1LDAuNzEsMS4zMjljMC4xOTYsMC40NzQsMC4zNCwwLjk1NywwLjQ0MywxLjQ1MmMwLjA5MywwLjQ5NCwwLjE0NCwwLjk5OSwwLjE0NCwxLjUwNCAgIGMwLDIuMDM4LTAuODI0LDQuMDI2LTIuMjU1LDUuNDU4Yy0wLjM2MSwwLjM2LTAuNzUyLDAuNjgtMS4xNzQsMC45NjdjLTAuNDIyLDAuMjc5LTAuODc1LDAuNTE1LTEuMzM5LDAuNzExICAgYy0wLjQ2NCwwLjE4NS0wLjk0OCwwLjM0LTEuNDQyLDAuNDMyQzE0Ni41MzIsMzgzLjQ0NSwxNDYuMDI3LDM4My40OTcsMTQ1LjUyMywzODMuNDk3eiIvPgoJPHBhdGggc3R5bGU9ImZpbGw6I0M5Qjg5RjsiIGQ9Ik0xNDUuNTIzLDM0Ni40MjRjLTIuMDI5LDAtNC4wMjctMC44MjUtNS40NTgtMi4yNTZjLTEuNDQyLTEuNDQyLTIuMjY1LTMuNDI5LTIuMjY1LTUuNDY4ICAgYzAtMC40OTUsMC4wNTEtMS4wMSwwLjE1NC0xLjUwNGMwLjEwMy0wLjQ5NSwwLjI0Ny0wLjk3OSwwLjQ0My0xLjQ1MmMwLjE4NS0wLjQ2NCwwLjQzMy0wLjkwNiwwLjcxMS0xLjMyOSAgIGMwLjI3OC0wLjQyMiwwLjU5Ny0wLjgxNCwwLjk1Ny0xLjE3NWMxLjc5Mi0xLjc5MSw0LjQ1OS0yLjYxNiw2Ljk3Mi0yLjExMWMwLjQ5NCwwLjEwNCwwLjk3OSwwLjI0NywxLjQ0MiwwLjQ0NCAgIGMwLjQ2MywwLjE5NSwwLjkxNiwwLjQzMiwxLjMyOCwwLjcxYzAuNDMzLDAuMjc5LDAuODI0LDAuNTk3LDEuMTg1LDAuOTU3YzAuMzUsMC4zNjEsMC42NjksMC43NTIsMC45NTcsMS4xNzUgICBjMC4yNzgsMC40MjIsMC41MTUsMC44NjUsMC43MSwxLjMyOWMwLjE5NiwwLjQ3NCwwLjM0LDAuOTU3LDAuNDQzLDEuNDUyYzAuMDkzLDAuNDk0LDAuMTQ0LDEuMDA5LDAuMTQ0LDEuNTA0ICAgYzAsMi4wMzgtMC44MjQsNC4wMjYtMi4yNTUsNS40NjhjLTAuMzYxLDAuMzUtMC43NTIsMC42OC0xLjE4NSwwLjk1N2MtMC40MTIsMC4yNzktMC44NjUsMC41MTUtMS4zMjgsMC43MTEgICBjLTAuNDY0LDAuMTk1LTAuOTQ4LDAuMzQtMS40NDIsMC40NDNDMTQ2LjUzMiwzNDYuMzcyLDE0Ni4wMjcsMzQ2LjQyNCwxNDUuNTIzLDM0Ni40MjR6Ii8+CjwvZz4KPHBhdGggc3R5bGU9ImZpbGw6IzkyQjYyMDsiIGQ9Ik00NTEuNTM3LDQ1MC4yNDFsLTQuODEzLTQuNjkyYy02Ljk3Mi02Ljc5NC0yLjg3Ny0xNS4wODItMi43ODUtMTguNTA2ICBjMC43MjgtNC4yNDYtMi4xNzYtOC4yNjQtNi40NTYtOC44ODZjLTMuMjY1LTAuOTYzLTEyLjUxMywwLjMwOS0xNi43OTgtOC4zNzZsLTIuOTgtNi4wMzhjLTEuOTEtMy44Ny02LjYyNS01LjQwMy0xMC40NDYtMy4zOTQgIGMtMy4yMDgsMS4xMzktOS45MjQsNy41OTUtMTguNDk1LDMuMDg5bC01Ljg3Ni0zLjA4OWMtMy43NjgtMS45OC04LjUxMi0wLjUyMy0xMC40NDUsMy4zOTRjLTEuOTQ3LDIuODQzLTMuNTk0LDEyLjA2Mi0xMy4xOSwxMy40NTYgIGwtNi41ODgsMC45NTdjLTQuMjcyLDAuNjIxLTcuMTg2LDQuNjMyLTYuNDU2LDguODg2YzAuMDksMy4zNzMsNC4xNDcsMTEuNzUtMi43ODQsMTguNTA2bC00LjgxMyw0LjY5MiAgYy0zLjA5MSwzLjAxMi0zLjA5LDcuOTcxLDAsMTAuOTgzbDQuODEzLDQuNjkxYzYuOTcxLDYuNzk1LDIuODc3LDE1LjA4MiwyLjc4NSwxOC41MDZjLTAuNzMsNC4yNTUsMi4xODUsOC4yNjUsNi40NTYsOC44ODYgIGMzLjI1OSwwLjk2MSwxMi41MTItMC4zMDksMTYuNzk4LDguMzc1YzEuOTc1LDIuODc0LDIuOTk5LDEwLjMxOCw5Ljg2OCwxMC4zMThjMi45NjQsMCw0LjMyOS0xLjUwMyw5LjQzMy0zLjk3NCAgYzguNjI4LTQuNTM3LDE1LjE5MiwxLjkxNSwxOC40OTUsMy4wODhjMy44MTQsMi4wMDUsOC41MzIsMC40ODQsMTAuNDQ2LTMuMzk0bDIuOTgtNi4wMzhjNC4zMS04LjczNCwxMy40NjYtNy4zOTIsMTYuNzk4LTguMzc1ICBjNC4yNzEtMC42MjEsNy4xODYtNC42MzEsNi40NTYtOC44ODZsLTEuMTE1LTYuNTAzYy0xLjY0OC05LjYwOSw2LjU2NS0xMy45MDUsOC43MTMtMTYuNjk0ICBDNDU0LjYyOCw0NTguMjExLDQ1NC42MjgsNDUzLjI1Myw0NTEuNTM3LDQ1MC4yNDF6Ii8+CjxwYXRoIHN0eWxlPSJmaWxsOiNFRkVGRUY7IiBkPSJNMjkwLjAxOCwwSDE3Ny4yNTVjLTYuNzk2LDAtMTIuMzU4LDUuNTYxLTEyLjM1OCwxMi4zNThjMCwwLjUxOCwwLDE3LjY0NSwwLDE2LjkyICBjMCwwLjE4OSwwLDcuNjI1LDAsNy4yMDljMCw2Ljc5Nyw1LjU2MSwxMi4zNTgsMTIuMzU4LDEyLjM1OGgxMTIuNzYzYzYuNzk3LDAsMTIuMzU4LTUuNTYxLDEyLjM1OC0xMi4zNThjMC0wLjE1OCwwLTcuNjc3LDAtNy4yMDkgIGMwLTAuMjM3LDAtMTAuMDc1LDAtOS43MTJ2LTcuMjA5QzMwMi4zNzUsNS41NjEsMjk2LjgxNSwwLDI5MC4wMTgsMHoiLz4KPHBhdGggc3R5bGU9ImZpbGw6I0RERERERDsiIGQ9Ik0xOTUuNzkxLDM2LjQ4N2MwLTAuMjI2LDAtMTYuMjQ4LDAtMTUuNjY5YzAtMC4xOTMsMC04LjkyNiwwLTguNDYgIEMxOTUuNzkxLDUuNTYxLDIwMS4zNTMsMCwyMDguMTQ5LDBoLTMwLjg5NGMtNi43OTYsMC0xMi4zNTgsNS41NjEtMTIuMzU4LDEyLjM1OGMwLDAuNTE4LDAsMTcuNjQ1LDAsMTYuOTJjMCwwLjE4OSwwLDcuNjI1LDAsNy4yMDkgIGMwLDYuNzk3LDUuNTYxLDEyLjM1OCwxMi4zNTgsMTIuMzU4aDMwLjg5NEMyMDEuMzUzLDQ4Ljg0NCwxOTUuNzkxLDQzLjI4MywxOTUuNzkxLDM2LjQ4N3oiLz4KPHBhdGggc3R5bGU9ImZpbGw6I0VGRUZFRjsiIGQ9Ik0zODIuNzg0LDQ3Ny40OTlsLTExLjEzMS0xMy44MjhjLTIuNjc1LTMuMzIzLTIuMTUtOC4xODUsMS4xNzQtMTAuODYgIGMzLjMyMy0yLjY3NCw4LjE4NS0yLjE0OSwxMC44NTksMS4xNzRsNS4zNDUsNi42NGwxNy44MzYtMjAuNDk0YzIuODAxLTMuMjE3LDcuNjc5LTMuNTU1LDEwLjg5Ny0wLjc1NSAgYzMuMjE4LDIuOCwzLjU1Niw3LjY3OCwwLjc1NiwxMC44OTZsLTIzLjg5MywyNy40NTVDMzkxLjQ3Myw0ODEuMzQ3LDM4NS44MDMsNDgxLjI0OSwzODIuNzg0LDQ3Ny40OTl6Ii8+CjxwYXRoIHN0eWxlPSJmaWxsOiM4M0EwMEI7IiBkPSJNMzg4LjE3Miw0MDMuMTI3bC01LjI4My0yLjc3N2MtMy43NjctMS45OC04LjUxMi0wLjUyMy0xMC40NDUsMy4zOTQgIGMtMS45NDcsMi44NDMtMy41OTQsMTIuMDYyLTEzLjE5LDEzLjQ1NmwtNi41ODgsMC45NTdjLTQuMjcyLDAuNjIxLTcuMTg2LDQuNjMyLTYuNDU2LDguODg2YzAuMDksMy4zNzMsNC4xNDcsMTEuNzQ5LTIuNzg0LDE4LjUwNiAgbC00LjgxMyw0LjY5MmMtMy4wOTEsMy4wMTItMy4wOSw3Ljk3MSwwLDEwLjk4M2w0LjgxMyw0LjY5MWM2Ljk3MSw2Ljc5NSwyLjg3NywxNS4wODIsMi43ODUsMTguNTA2ICBjLTAuNzMsNC4yNTUsMi4xODQsOC4yNjUsNi40NTYsOC44ODZjMy4yNTksMC45NjEsMTIuNTEyLTAuMzA4LDE2Ljc5OCw4LjM3NWMxLjk3NSwyLjg3NCwyLjk5OSwxMC4zMTgsOS44NjgsMTAuMzE4ICBjMi45NjQsMCw0LjMyOS0xLjUwMyw5LjQzMy0zLjk3NGM4LjYyOC00LjUzNywxNS4xOTIsMS45MTUsMTguNDk1LDMuMDg4YzMuODE0LDIuMDA1LDguNTMyLDAuNDg0LDEwLjQ0Ni0zLjM5NGwyLjE4NC00LjQyNSAgQzM2OC40MjUsNTAwLjE5LDM0Ny45NSw0MzUuMDgzLDM4OC4xNzIsNDAzLjEyN3oiLz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==" /></div>
                </a>
            </div>
            <div class="row">
                <div class="collapse" id="collapseExample'.$i.'">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col" class="manifest-table-title-blue">Serial</th>
                            <th scope="col" class="manifest-table-title-blue">HAWB</th>
                            <th scope="col" class="manifest-table-title-blue">Org</th>
                            <th scope="col" class="manifest-table-title-blue">Sender</th>
                            <th scope="col" class="manifest-table-title-blue">Receiver</th>
                            <th scope="col" class="manifest-table-title-blue">Shpt Info</th>
                            <th scope="col" class="manifest-table-title-blue">Service Info</th>
                        </tr>
                        </thead>
                        <tbody>';
            $array_hawb = $array[$i]['hawb'];
            for ($j = 0; $j < count($array_hawb); $j++) {
                $array_hawb_j = $array_hawb[$j];
                $body .= '
                        <tr>
                            <th scope="row" class="manifest-table-blue">'.($j+1).'</th>
                            <td class="manifest-table-blue">'.$array_hawb_j['id'].'</td>
                            <td class="manifest-table-blue">'.$array_hawb_j['origin'].'</td>
                            <td class="manifest-table-blue">'.$array_hawb_j['snipper_id']['name_s'].'<br>'
                                .$array_hawb_j['snipper_id']['address'].'<br>'
                                .$array_hawb_j['snipper_id']['contry'].'<br>'
                                .$array_hawb_j['snipper_id']['city'].'<br> Tel:'
                                .$array_hawb_j['snipper_id']['phone']
                                .'</td>
                            <td class="manifest-table-blue">'.$array_hawb_j['consignee_id']['name_c'].'<br>'
                                .$array_hawb_j['consignee_id']['address'].'<br>'
                                .$array_hawb_j['consignee_id']['contry'].'<br>'
                                .$array_hawb_j['consignee_id']['city'].'<br> Tel:'
                                .$array_hawb_j['consignee_id']['phone']
                            .'</td>
                            <td class="manifest-table-blue">'.'Wgt: '.$array_hawb_j['box_id']['charg_wt'].'<br>'
                                .'Pcs: '.$array_hawb_j['box_id']['pcs'].'<br>'
                                .'Ask Goods: <br>'
                                .'Customs Value: '.$array_hawb_j['box_id']['goods_value'].'<br>'
                                .'Destination: '.$array_hawb_j['consignee_id']['contry'].'<br>'
                                .'
                            </td>
                            <td class="manifest-table-blue">
                                '.'Type: '.$array_hawb_j['box_id']['product_2'].'<br>'
                                .'CC Amt Ask: '.$array_hawb_j['box_id']['pcs'].'<br>'
                            .'</td>
                        </tr>
                        
                        ';
                unset($array_hawb_j);
            }
                $body.='</tbody>
                    </table>

                </div>
            </div>
         </div>
         ';
            }
        //}

        return $body;
    }
}