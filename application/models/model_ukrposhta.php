<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 5/25/2018
 * Time: 10:17 AM
 */

class model_ukrposhta extends Model{
    private $json;
    private $connect;

    public function __construct(){
        $this->connect =  new Database(HOST,DB,USER,PASS);
    }

    private function makeAddressSender(){
        $this->json = [
            "postcode"=> "07401",
            "country"=> "UA",
            "region"=> "Київська",
            "city"=> "Бровари",
            "district"=> "Київський",
            "street"=> "Котляревського",
            "houseNumber"=> "12",
            "apartmentNumber"=>"33"
        ];
    }

    private function makeAddressRecipient(){

    }

    private function makeClientSender(){

    }

    private function makeClientRecipient(){
        $this->json[] = [
            "name" => "ТОВ Експресс Банк",
            "uniqueRegistrationNumber" => "0035",
            "addressId" => 56922,
            "phoneNumber"=> "067 123 12 34",
            "individual"=> true,
            "bankCode"=> "123000",
            "bankAccount"=> "111000222000999",
            "resident" => true,
            "edrpou" => "20053145",
            "email"=> "test@test.com",
            "tin" => "3245896321"
        ];
    }

    private function makeShipment(){

    }
    public function example(){
        $this->makeAddressSender();
        $this->makeClientRecipient();
        $params=['name'=>'John', 'surname'=>'Doe', 'age'=>36];
        $defaults = array(
        CURLOPT_URL => 'https://www.ukrposhta.ua/ecom/0.0.1//addresses',
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => $params,
        CURLOPT_HEADER => [
            'Authorization: Bearer ba5378df-985e-49c5-9cf3-d222fa60aa68',
            'Content-Type: application/json'
        ]
        );
        $ch = curl_init();
        curl_setopt_array($ch, $defaults);
        curl_exec($ch);
        exit;
        return json_encode($this->json,JSON_UNESCAPED_UNICODE);
    }
}