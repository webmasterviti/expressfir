<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 30.05.2018
 * Time: 21:48
 */

class model_mawb_edit extends Model
{
    private $connect;

    public function __construct(){
        $this->connect =  new Database(HOST,DB,USER,PASS);
    }
    function seaview ($search)
    {

        $sql = "SELECT * FROM mawb WHERE id = $search";
        try {
            $result = $this->connect->db->query($sql);
            $row = $result->fetch();
            $ttc_bags_weight = explode(" ", $row['ttc_bags_weight']);
            $hawbs_cube = explode(" ", $row['hawbs_cube']);

            $exspress_id = $row['exspress_id'];
            $sql1 = "SELECT * FROM express_mawb WHERE id = $exspress_id";
            $result = $this->connect->db->query($sql1);
            $row1 = $result->fetch();

            $text .= "
<form action=\"mawb_edit\" method=\"post\">
            <div class=\"row\">
<div class=\"col-md-2 mt-10\">
    <br>
    <b>Master Airwaybill</b>
</div>
<div class=\"col-md-2\">
    <label for=\"org_entity\">Org.Entity</label>
    <input class=\"form-control\" id=\"org_entity\" name=\"org_entity\" placeholder=\"\" value=\"". $row['org_entity']."\" required=\"\"
           type=\"text\" >
</div>
<div class=\"col-md-1\">
    <label for=\"carrier\">Carrier</label>
    <input class=\"form-control\" id=\"carrier\" name=\"carrier\" placeholder=\"\" value=\"". $row['carrier']."\" required=\"\"
           type=\"text\" >
</div>
<div class=\"col-md-1\">
    <br>
    <button type=\"button\" class=\"btn btn-info\">...</button>
</div>
<div class=\"col-md-2\">
    <label for=\"org_port\">Org.Port</label><br>
    <select class=\"form-control\" id=\"org_port\" name=\"org_port\" required=\"\" >
        <option value=\"". $row['org_port']."\" selected>". $row['org_port']."</option>
    </select>
</div>
<div class=\"col-md-2\">
    <label for=\"mawb_no\">MAWB No.</label>
    <input class=\"form-control\" id=\"mawb_no\" name=\"mawb_no\" placeholder=\"\" value=\"". $row['mawn_no']."\" required=\"\"
           type=\"text\" >
</div>
<div class=\"col-md-2\">
    <label for=\"sub\">Sub</label>
    <input class=\"form-control\" id=\"sub\" name=\"sub\" placeholder=\"\" value=\"". $row['sub']."\" required=\"\"
           type=\"text\" >
</div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"origin_branch\">Origin Branch</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <select class=\"form-control\" id=\"origin_branch\" name=\"origin_branch\" required=\"\" >
            <option value=\"". $row['origin_branch']."\" selected>". $row['origin_branch']."</option>
        </select>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"destination_port\">Destination Port</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"destination_port\" name=\"destination_port\" placeholder=\"\" value=\"". $row['destination_port']."\" required=\"\"
               type=\"text\" >
    </div>
    <div class=\"col-md-2 pt-10\">
        <button type=\"button\" class=\"btn btn-info\">...</button>
    </div>
</div>

<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"destination_entity\">Destination Entity</label>
    </div>
    <div class=\"col-md-3 pt-10\">
        <select class=\"form-control\" id=\"destination_entity\" name=\"destination_entity\" required=\"\" >
            <option value=\"". $row['destination_entity']."\" selected>Aramex/JFK</option>
        </select>
    </div>
    <div class=\"col-md-1 pt-10\">
        <label for=\"branch\">Branch</label>
    </div>
    <div class=\"col-md-3 pt-10\">
        <select class=\"form-control\" id=\"branch\" name=\"branch\" required=\"\" >
            <option value=\"". $row['branch']."\" selected>". $row['branch']."</option>
        </select>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"mode_of_transport\">Mode of Transport</label>
    </div>
    <div class=\"col-md-3 pt-10\">
        <select class=\"form-control\" id=\"mode_of_transport\" name=\"mode_of_transport\" required=\"\" >
            <option value=\"". $row['mode_of_transport']."\" selected>". $row['mode_of_transport']."</option>
        </select>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"no_of_baby_bags\">No. of Baby Bags</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"no_of_baby_bags\" name=\"no_of_baby_bags\" placeholder=\"\" value=\"". $row['no_of_baby_bags']."\" required=\"\"
               type=\"text\" >
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"no_of_master_bags\">No. of Master Bags(TTC)</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"no_of_master_bags\" name=\"no_of_master_bags\" placeholder=\"\" value=\"". $row['no_of_master_bags']."\" required=\"\"
               type=\"text\" >
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"ttc_bags_weight\">TTC Bags Weight</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"ttc_bags_weight\" name=\"ttc_bags_weight\" placeholder=\"\" value=\"". $ttc_bags_weight[0]."\" required=\"\"
               type=\"text\" >
    </div>
    <div class=\"col-md-2 pt-10\">
        <select class=\"form-control\" id=\"ttc_bags_weight_iz\" name=\"ttc_bags_weight_2\" required=\"\" >
            <option value=\"". $ttc_bags_weight[1]."\" selected>". $ttc_bags_weight[1]."</option>
        </select>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"actual_weight\">Actual Weight</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"actual_weight\" name=\"actual_weight\" placeholder=\"\" value=\"". $row['actual_weight']."\" required=\"\"
               type=\"text\" >
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"hawbs_wight\">HAWBs Weight</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"hawbs_wight\" name=\"hawbs_wight\" placeholder=\"\" value=\"". $row['hawbs_weight']."\" required=\"\"
               type=\"text\" >
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"hawbs_cube\">HAWBs Cube</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"hawbs_cube\" name=\"hawbs_cube\" placeholder=\"\" value=\"". $hawbs_cube[0]."\" required=\"\"
               type=\"text\" >
    </div>
    <div class=\"col-md-2 pt-10\">
        <select class=\"form-control\" id=\"hawbs_cube_iz\" name=\"hawbs_cube_2\" required=\"\" >
            <option value=\"". $hawbs_cube[1]."\" selected>". $hawbs_cube[1]."</option>
        </select>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"remarks\">Remarks</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"remarks\" name=\"remarks\" placeholder=\"\" value=\"". $row['remarks']."\" required=\"\"
               type=\"text\" >
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"linehaur_supplier\">Linehaur supplier</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <select class=\"form-control\" id=\"linehaur_supplier\" name=\"linehaur_supplier\" required=\"\" >
            <option value=\"". $row['linehaul_supplier']."\" selected>". $row['linehaul_supplier']."</option>
        </select>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"srr\">SRR</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"srr\" name=\"srr\" placeholder=\"\" value=\"". $row['srr']."\" required=\"\"
               type=\"text\" >
    </div>
</div>

<div class=\"row\">
    <div class=\"col-md-2 pt-10\">
        <label for=\"custom_reference\">Custom Reference</label>
    </div>
    <div class=\"col-md-2 pt-10\">
        <input class=\"form-control\" id=\"custom_reference\" name=\"custom_reference\" placeholder=\"\" value=\"". $row['custom_reference']."\" required=\"\"
               type=\"text\" >
    </div>
</div>

<hr style=\"border:1px solid darkgrey\">

<div class=\"row\">
    <div class=\"col-md-1 mt-10\">
        <br>
        <b>Flight</b>
    </div>
    <div class=\"col-md-2\">
        <label for=\"al_code\">A/L Code</label>
        <div class=\"row\">
            <div class=\"col-md-6\">
                <input class=\"form-control\" id=\"al_code\" name=\"al_code\" placeholder=\"\" value=\"". $row1['al_code']."\" required=\"\"
                       type=\"text\" >
            </div>
            <div class=\"col-md-6\">
                <button type=\"button\" class=\"btn btn-info\">...</button>
            </div>
        </div>
    </div>
    <div class=\"col-md-1\">
        <label for=\"flt_no\">Flt No</label>
        <input class=\"form-control\" id=\"flt_no\" name=\"flt_no\" placeholder=\"\" value=\"". $row1['flt_no']."\" required=\"\"
               type=\"text\" >
    </div>
    <div class=\"col-md-3\">
        <label for=\"etd_date\">ETD(m/d/y h:mm)</label><br>
        <div class=\"row\">
            <div class=\"col-md-6\">
                <input class=\"form-control\" id=\"etd_date\" name=\"etd_date\" placeholder=\"\" value=\"". $row1['etd']."\" required=\"\"
                       type=\"text\" >
            </div>
            <div class=\"col-md-2\">
                <button type=\"button\" class=\"btn btn-info\">...</button>
            </div>
            <div class=\"col-md-4\">
                <input class=\"form-control\" name=\"etd_time\" placeholder=\"\" value=\"". $row1['etd_time']."\" required=\"\"
                       type=\"text\" >
            </div>
        </div>
    </div>
    <div class=\"col-md-3\">
        <label for=\"eta_date\">ETA(m/d/y h:mm)</label><br>
        <div class=\"row\">
            <div class=\"col-md-6\">
                <input class=\"form-control\" id=\"eta_date\" name=\"eta_date\" placeholder=\"\" value=\"". $row1['eta']."\" required=\"\"
                       type=\"text\" >
            </div>
            <div class=\"col-md-2\">
                <button type=\"button\" class=\"btn btn-info\">...</button>
            </div>
            <div class=\"col-md-4\">
                <input class=\"form-control\" name=\"eta_time\" placeholder=\"\" value=\"". $row1['eta_time']."\" required=\"\"
                       type=\"text\" >
            </div>
        </div>
    </div>
</div>
<hr style=\"border:1px solid darkgrey\">
<div class=\"row\">
<button class=\"btn btn-primary btn-lg btn-block mt-10\" type=\"submit\">Continue to checkout</button>
            </form>
        ";

            return  $text;
        } catch (PDOException $e) {
            return "попробуйте другой номер накладной";
        }
    }

    function update($se,$e1,$e2,$e3,$e4,$e5,$e6,$m1,$m2,$m3,$m4,$m5,$m6,$m7,$m8,$m9,$m10,$m11,$m12,$m13,$m14,$m15,$m16,$m17,$m18,$m19,$sub_post){
        $sql = "SELECT * FROM mawb WHERE id = $se";
        $result =  $this->connect->db->query($sql);
        $row = $result->fetch();
        $exspress =  $row["exspress_id"];


        $statement =  $this->connect->db->prepare("UPDATE mawb SET org_entity=:org_entity, org_port=:org_port, carrier=:carrier, mawn_no=:mawn_no, origin_branch=:origin_branch, destination_port=:destination_port, destination_entity=:destination_entity,branch=:branch,mode_of_transport=:mode_of_transport,no_of_baby_bags=:no_of_baby_bags, no_of_master_bags=:no_of_master_bags,ttc_bags_weight=:ttc_bags_weight,actual_weight=:actual_weight,hawbs_weight=:hawbs_weight,hawbs_cube=:hawbs_cube,remarks=:remarks,linehaul_supplier=:linehaul_supplier,srr=:srr,custom_reference=:custom_reference,sub=:sub WHERE id= $se");

        $statement->bindParam(':org_entity',  $m1, PDO::PARAM_STR);
        $statement->bindParam(':org_port',  $m2, PDO::PARAM_STR);
        $statement->bindParam(':carrier',  $m3, PDO::PARAM_STR);
        $statement->bindParam(':mawn_no',  $m4, PDO::PARAM_STR);
        $statement->bindParam(':origin_branch',  $m5, PDO::PARAM_STR);
        $statement->bindParam(':destination_port',  $m6, PDO::PARAM_STR);
        $statement->bindParam(':destination_entity',  $m7, PDO::PARAM_STR);
        $statement->bindParam(':branch',  $m8, PDO::PARAM_STR);
        $statement->bindParam(':mode_of_transport',  $m9, PDO::PARAM_STR);
        $statement->bindParam(':no_of_baby_bags',  $m10, PDO::PARAM_STR);
        $statement->bindParam(':no_of_master_bags',  $m11, PDO::PARAM_STR);
        $statement->bindParam(':ttc_bags_weight',  $m12, PDO::PARAM_STR);
        $statement->bindParam(':actual_weight',  $m13, PDO::PARAM_STR);
        $statement->bindParam(':hawbs_weight',  $m14, PDO::PARAM_STR);
        $statement->bindParam(':hawbs_cube',  $m15, PDO::PARAM_STR);
        $statement->bindParam(':remarks',  $m16, PDO::PARAM_STR);
        $statement->bindParam(':linehaul_supplier',  $m17, PDO::PARAM_STR);
        $statement->bindParam(':srr',  $m18, PDO::PARAM_STR);
        $statement->bindParam(':custom_reference',  $m19, PDO::PARAM_STR);
        $statement->bindParam(':sub',  $sub_post, PDO::PARAM_STR);
        $statement->execute();



        $statement =  $this->connect->db->prepare("UPDATE express_mawb SET al_code=:al_code, flt_no=:flt_no, etd=:etd, etd_time=:etd_time,eta=:eta,eta_time=:eta_time WHERE id= $exspress");

        $statement->bindParam(':al_code',  $e1, PDO::PARAM_STR);
        $statement->bindParam(':flt_no',  $e2, PDO::PARAM_STR);
        $statement->bindParam(':etd',  $e3, PDO::PARAM_STR);
        $statement->bindParam(':etd_time',  $e4, PDO::PARAM_STR);
        $statement->bindParam(':eta',  $e5, PDO::PARAM_STR);
        $statement->bindParam(':eta_time',  $e6, PDO::PARAM_STR);
        $statement->execute();
    }

}
