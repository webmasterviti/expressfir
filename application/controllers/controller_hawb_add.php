<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 17.05.2018
 * Time: 22:44
 */
class controller_hawb_add extends Controller
{

    function __construct()
    {
        $this->model = new Model_hawb_add();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        $data = $this->model->allInfoUser();
        if($_POST){
//            payment
            $payment = $_POST["payment_id"] . " " . $_POST["payment_id_2"];
            $collect_amt= $_POST["collect_amt"] . " " . $_POST["collect_amt_2"];
            $cod_amt = $_POST["cod_amt"] . " " . $_POST["cod_amt_2"];
            $cash_amt = $_POST["cash_amt"] . " " . $_POST["cash_amt_2"];

//            box
            $good_value = $_POST["goods_value"] . " " . $_POST["goods_value_2"];
            $shild_value_id = $_POST["shield_value"] . " " . $_POST["shield_value_2"];
            $charg_wt = $_POST["charg_wt"] . " " . $_POST["charg_wt_iden"];
            $cube = $_POST["cube"]  . " " . $_POST["cube_iden"];
            $product_1 = $_POST["product_exp"];
            $product_2 = $_POST["product_ppx"];
            $remarks = $_POST["remarks"]  . " " . $_POST["remarks_val"];

            $this->model->addInTable($_POST["origin"],$_POST["destination"],$_POST["manifest"],$good_value ,$_POST["pickup_by"],$shild_value_id, $_POST["weight"],$charg_wt, $_POST["pcs"],$cube,$_POST["factor"],$product_1,$product_2,$_POST["due_dt"], $_POST['pickup_dt'],$_POST['loc'],$remarks,$_POST['goods_org'],$_POST['description'],$_POST["account"], $_POST["country"], $_POST["phone"],$_POST["phone2"],$_POST["name"],$_POST["sent_by"],$_POST["ref"],$_POST["ref2"],$_POST["address"],$_POST["city"],$_POST["state"],$_POST["zip"],$_POST["accountc"], $_POST["countryc"], $_POST["phonec"],$_POST["phone2c"],$_POST["name_c"],$_POST["attn"],$_POST["refc"],$_POST["ref2c"],$_POST["addressc"],$_POST["cityc"],$_POST["statec"],$_POST["zipc"], $payment, $_POST["srn_no"], $_POST["collection_ref"],$collect_amt,$cod_amt,$cash_amt,$_POST["awb_reference"]);

            $this->view->generate('admin/hawb_add_view.php', 'admin/template_view.php', $data);
        } else{
            $this->view->generate('admin/hawb_add_view.php', 'admin/template_view.php',$data);
        }
    }
}