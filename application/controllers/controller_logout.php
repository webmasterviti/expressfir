<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 11.05.2018
 * Time: 22:00
 */
class controller_logout extends Controller
{
    function __construct()
    {
        $this->view = new View();
        $this->model = new model_logout();
    }
    function action_index()
    {
        $this->model->logout();
        $this->view->generate('main_view.php', 'template_index_view.php');
    }
}