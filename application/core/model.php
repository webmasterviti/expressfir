<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 03.01.2018
 * Time: 23:30
 */


class Model
{
    public function get_data(){

    }

    public function auth($login, $pass){
        $resSession = $this->authSession();

        if (!$resSession['Result']) {
            $pdo = new Database(HOST, DB, USER, PASS);
            $resultQuery = $pdo->getAllUser($login);
            if (!$resultQuery) {
                return [
                    'Result' => FALSE,
                    'Text' => 'Login not found'
                ];
            }
            if ($resultQuery['password'] != $pass) {
                return [
                    'Result' => FALSE,
                    'Text' => 'Password does not match'
                ];
            }
            $_SESSION['id'] = $resultQuery['id'];
            $_SESSION['login'] = $resultQuery['login'];
            $_SESSION['password'] = $resultQuery['password'];
        }
        return [
            'Result' => TRUE,
            'Text' => 'Coincidence'
        ];
    }
    public function authSession(){
        if(!isset($_SESSION)){
            return FALSE;
        }
        $pdo =  new Database(HOST,DB,USER,PASS);
        $resultQuery = $pdo->getAllUser($_SESSION['login']);
        if (!$resultQuery){
            return [
                'Result' => FALSE,
                'Text'=>'Login not found'
            ];
        }

        if ($_SESSION['password'] != $resultQuery['password']){
            return [
                'Result' => FALSE,
                'Text' =>'Password does not match'
            ];
        }
        return [
            'Result' => TRUE,
            'Text' => 'Session data == Db data'
        ];
    }

    private function userLaw($userLaw){
        switch ($userLaw){
            case 'level_1':
                return 1;
            case 'level_2':
                return 2;
            case 'level_3';
                return 3;
        }
        return 0;
    }
    public function allInfoUser(){
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT id, login, password,firstname,lastname,law FROM user WHERE id = :id');
        $query->execute([':id' => $_SESSION['id']]);
        $resultQuery = $query->fetchAll();
        $resultQuery[0]['userLaw'] = $this->userLaw($resultQuery[0]['law']);
        unset($resultQuery[0]['law']);
        return $resultQuery[0];
    }
    public function allInfoHawb(){
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT * FROM hawb');
        $query->execute();
        $resultQuery = $query->fetchAll();
        for($i = 0;$i < count($resultQuery);$i++){
            $query = $connect->db->prepare('SELECT name_s,phone FROM snipper WHERE id ='. $resultQuery[$i]['snipper_id']);
            $query->execute();
            $result_snipper = $query->fetchAll();
            ///////
            $resultQuery[$i]['snipper_id'] = $result_snipper[0];
            $query = $connect->db->prepare('SELECT name_c,phone FROM consignee WHERE id ='. $resultQuery[$i]['consignee_id']);
            $query->execute();
            $result_consignee = $query->fetchAll();
            $resultQuery[$i]['consignee_id'] = $result_consignee[0];
        }

        return $resultQuery;
    }
    public function partInfoHawb(){
        $connect = new Database(HOST, DB, USER, PASS);
        $query = $connect->db->prepare('SELECT * FROM hawb WHERE mawb_id IS NULL');
        $query->execute();
        $resultQuery = $query->fetchAll();
        for($i = 0;$i < count($resultQuery);$i++){
            $query = $connect->db->prepare('SELECT name_s,phone FROM snipper WHERE id ='. $resultQuery[$i]['snipper_id']);
            $query->execute();
            $result_snipper = $query->fetchAll();
            ///////
            $resultQuery[$i]['snipper_id'] = $result_snipper[0];
            $query = $connect->db->prepare('SELECT name_c,phone FROM consignee WHERE id ='. $resultQuery[$i]['consignee_id']);
            $query->execute();
            $result_consignee = $query->fetchAll();
            $resultQuery[$i]['consignee_id'] = $result_consignee[0];
        }
        return $resultQuery;
    }
}